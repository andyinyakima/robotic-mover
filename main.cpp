#include "robotic_mover.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    robotic_mover w;
    w.show();

    return a.exec();
}
