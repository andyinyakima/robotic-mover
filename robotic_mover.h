#ifndef ROBOTIC_MOVER_H
#define ROBOTIC_MOVER_H

#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QtWidgets>
#include <QFileSystemWatcher>
#include <QDateTime>
#include <QTimer>
#include <QThread>
#include <QProcess>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <unistd.h>


namespace Ui {
class robotic_mover;
}

class robotic_mover : public QMainWindow
{
    Q_OBJECT

public:
    explicit robotic_mover(QWidget *parent = 0);
    ~robotic_mover();

private slots:

    void sre();

    void readsre();

    void stopsre();

    void parse();

    void greet();

    void flitefile();

    void mic_control();

    void edit_ahead();

    void edit_backup();

    void hard_turn();

    void ease_turn();

    void do_turn();

    void parse_needs();

    void parse_speaker();

    void write_to_arduino(QString);

    void stop_progress();

    void setup_boswasi();



private:
        bool forward_flag = true;

        int timer_flag =0;
        int num_digit;
        int num_result;
        int talker_flag=0;

        QString asrline;  // This is a global string for speech recognition engine
        QString ttsline;  // This is the string that TTS will say.
        QString ttstemp;
        QString homepath;
        QString filepath;
        QString voice;
        QString load_text;

        QStringList vnames;
        QStringList vinitials;

      //  QTimer *timer;

        Ui::robotic_mover *ui;
        QSerialPort *arduino;
        static const quint16 arduino_leo_vendor_id =9025;
        static const quint16 arduino_leo_product_id=32822;
        QString arduino_port_name;
        bool arduino_is_available;

};

#endif // ROBOTIC_MOVER_H
