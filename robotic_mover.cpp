#include "robotic_mover.h"
#include "ui_robotic_mover.h"

/*
 * robotic-mover moves robot wheel base
 *
 * version .03 interface to robotic_mover
 * version .04 rudimentary capture of speech
 * version .05 tts and some commands, back to boswasi
 * version .06 pull in keywords to textedit
 * version .07 arduino basic moves
 * version .08 clean up gui and code after saving a copy of robotic-mover-feb4
 * version .09 changed keyword REVERSE to keyword BACKUP
 * version .10 modified GUI
 * version .11 use ease left and ease right
 * version .12 added mic control
 * version .13 added STAND DOWN and LISTEN UP to mic
 * version .14 added more safeties for noise and rogue calls
 */

// placement of the QObject and QProcess is critical
// it has to be in the include section...continued below
QObject *parent;

QProcess *asrin = new QProcess (parent);

// .....and above this section

robotic_mover::robotic_mover(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::robotic_mover)



{
     arduino =new QSerialPort;
     arduino_is_available = false;
     arduino_port_name = "";
    /*
     *
     *
    // the code below is a way to find available serialports and id them
    // this might be good to know so that it may be possible to run multiple arduinos

    qDebug() << "Num of Ports" << QSerialPortInfo::availablePorts().length();
        foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
            qDebug() << "Has Vendor ID: " << serialPortInfo.hasVendorIdentifier();
            if(serialPortInfo.hasVendorIdentifier())  {
                qDebug() << "Vendor ID: " << serialPortInfo.vendorIdentifier();
            }
            qDebug() << "Has Product ID: " << serialPortInfo.hasProductIdentifier();
            if(serialPortInfo.hasProductIdentifier())  {
                qDebug() << "Vendor ID: " << serialPortInfo.productIdentifier();
            }
        }
    */
     foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
         if(serialPortInfo.hasVendorIdentifier() && serialPortInfo.hasProductIdentifier()){
             if(serialPortInfo.vendorIdentifier()==arduino_leo_vendor_id){
                 if(serialPortInfo.productIdentifier()==arduino_leo_product_id){
                     arduino_port_name = serialPortInfo.portName();
                     arduino_is_available = true;
                    //  ui->usbport_lineEdit->insert(arduino_port_name);
                 }
             }
         }

     if(arduino_is_available){
      //  open and configure serial port
        arduino->setPortName(arduino_port_name);
        arduino->open(QSerialPort::WriteOnly);
        arduino->setBaudRate(QSerialPort::Baud9600);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);
        arduino->setFlowControl(QSerialPort::NoFlowControl);


     }
     else{
         //error for no arduino
         QMessageBox::warning(this,"Port Error", "Arduino Leo doesn't seem to be on Line" );
     }

    homepath=QDir::homePath();


    ui->setupUi(this);
    ui->usbport_lineEdit->insert(arduino_port_name);
    ui->sre_lineEdit->clear();
    ui->sre_lineEdit->insert("This is Beta version 0.14 of robotic-mover!");
    ttstemp=ui->sre_lineEdit->text();


    QFile filein1(homepath+"/robotic-mover/KEYWORDS");
    filein1.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream streamin1(&filein1);
    while (!streamin1.atEnd()) {
           load_text = streamin1.readLine();
       //    ui->textBrowser->clear();
           ui->textEdit->append(load_text);
    }




    ttsline=ui->sre_lineEdit->text();

    QFile filein(homepath+"/bos-jt/voice.txt");
    filein.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream streamin(&filein);
    while (!streamin.atEnd()) {
                voice = streamin.readLine();
                voice.prepend(homepath+"/");

     }

    talker_flag=0;

    greet();

    sre();


}
robotic_mover::~robotic_mover()
{
    delete ui;
    asrin->kill();  // this is needed to kill instance of julius
                    // other wise there will be too many copies.
                    // also close the arduino connection
    if(arduino->isOpen())
        arduino->close();

}


void robotic_mover::sre()
{
   // let's set up julius
    QString confpath = QDir::homePath();
    confpath.append("/robotic-mover.jconf");

    QString prog = "julius";
    QString conf = "-C";
    QStringList argu;

    argu<<conf<<confpath;
    asrin->setEnvironment(QProcess::systemEnvironment());
    asrin->setProcessChannelMode(QProcess::MergedChannels);

    asrin->start(prog,argu);

    connect(asrin,SIGNAL(readyReadStandardOutput()),this,SLOT(readsre()));
    connect(asrin,SIGNAL(finished(int)),this,SLOT(stopsre()));

}

void robotic_mover::readsre()
{
    int foundstart;
    int foundend;

    QString line;
    QByteArray pile;
    QStringList lines;

    QProcess *asrin = dynamic_cast<QProcess *>(sender());


    if(asrin)
    {

       pile=asrin->readAllStandardOutput();
       lines=QString(pile).split("\n");
       foreach (line, lines) {
           if(line.contains("sentence1"))
           {
               foundstart = line.indexOf("<s>")+3;
               foundend = line.indexOf("</s>");
               line.resize(foundend);
               line.remove(0,foundstart);

               asrline=line;

           ui->sre_lineEdit->insert(asrline);
           }

       }
        ui->sre_lineEdit->insert(asrin->readAllStandardOutput());

    }
    parse();
}

void robotic_mover::stopsre()
{
 /*
    asrin->waitForFinished();
    if(asrin->state()!= QProcess::NotRunning)
        asrin->kill();
*/
}


void robotic_mover::parse()
{
    if(asrline.contains("STAND")==true)
    {
        asrline.remove(0,6);
        mic_control();
    }


    if(asrline.contains("STOP")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        stop_progress();
    }


    if(asrline.contains("HALT")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        stop_progress();
    }

    if(asrline.contains("FORWARD")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,8);
        forward_flag=true;
        edit_ahead();
    }

    if(asrline.contains("AHEAD")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,6);
        forward_flag=true;
        edit_ahead();
    }

    if(asrline.contains("REVERSE")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,8);
        forward_flag=false;
        edit_backup();
    }

    if(asrline.contains("BACKUP")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,7);
        forward_flag=false;
        edit_backup();
    }

    if(asrline.contains("TURN")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        do_turn();

    }

    if(asrline.contains("EASE")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        ease_turn();

    }

    if(asrline.contains("HARD")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,5);
        hard_turn();

    }

    if(asrline.contains("SPEAKER")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,8);
        parse_speaker();
    }

    if(asrline.contains("BOS")==true && ui->mic_on_checkBox->isChecked()==true)
    {
        asrline.remove(0,4);
        parse_needs();
    }
    if(asrline.contains("LISTEN")==true)
    {
        asrline.remove(0,7);
        mic_control();
    }
}

void robotic_mover::edit_ahead()
{

    QString push;
    push.clear();
    ui->lineEdit_result->clear();
    QString line = asrline;
    bool ok;


   //     line.replace("POINT",".");


   //        line.replace("ZERO ZERO",QString::number(0));

           line.replace("ONE ONE",QString::number(1));
           line.replace("TWO TWO",QString::number(2));
           line.replace("THREE THREE",QString::number(3));
           line.replace("FOUR FOUR",QString::number(4));
           line.replace("FIVE FIVE",QString::number(5));
           line.replace("SIX SIX",QString::number(6));
           line.replace("SEVEN SEVEN",QString::number(7));
           line.replace("EIGHT EIGHT",QString::number(8));
           line.replace("NINE NINE",QString::number(9));


           line.replace(" ","");
           push.prepend(line);
           num_digit = push.toFloat(&ok);
           num_result=num_digit;

           if(num_result!=0)
           {
            line=QString::number(num_result);
            ui->lineEdit_result->clear();
            ui->lineEdit_result->insert(line);

            write_to_arduino("F"+line);

            if(ui->speaker_on_checkBox->isChecked()==true)
            {
                ttsline="Moving forward at a speed of "+line;
                greet();
            }
           }
}

void robotic_mover::edit_backup()
{

    QString append;
    append.clear();
    //append=ui->lineEdit_result->text();
    ui->lineEdit_result->clear();

    QString line = asrline;
    bool ok;


       // line.replace("POINT",".");


       //    line.replace("ZERO",QString::number(0));

           line.replace("ONE ONE",QString::number(1));
           line.replace("TWO TWO",QString::number(2));
           line.replace("THREE THREE",QString::number(3));
           line.replace("FOUR FOUR",QString::number(4));
           line.replace("FIVE FIVE",QString::number(5));
           line.replace("SIX SIX",QString::number(6));
           line.replace("SEVEN SEVEN",QString::number(7));
           line.replace("EIGHT EIGHT",QString::number(8));
           line.replace("NINE NINE",QString::number(9));


           line.replace(" ","");
           append.append(line);
           num_digit = append.toFloat(&ok);
           num_result=num_digit;

           if(num_result!=0)
           {
            line=QString::number(num_result);
            ui->lineEdit_result->clear();
            ui->lineEdit_result->insert(line);

            write_to_arduino("B"+line);
            if(ui->speaker_on_checkBox->isChecked()==true)
            {
                ttsline="Backup at a speed of "+line;
                greet();
            }
           }
}

void robotic_mover::parse_needs()
{


    QString line = asrline;
    asrline.clear();



    if(line.contains("WA")==true)
    {

        write_to_arduino("S");
        // add a one shot timer delay to allow write to arduino
        QTimer::singleShot(200, this, SLOT(setup_boswasi()));
    }


}

void robotic_mover::greet()
{
     QString line;
    if(ttsline!=NULL)
    {
        QFile file(homepath+"/bos-jt/flite_file.txt");
            if(!file.open (QIODevice::WriteOnly | QIODevice::Text))
                return;

             QTextStream out(&file);
             line=out.readLine();
             out<<ttsline;
             file.close();
             filepath=homepath+"/bos-jt/flite_file.txt";


    //  if(talker_flag!=0)
    //   flitetalk();
    //  else if(talker_flag==0)
        flitefile();
    }

}

void robotic_mover::write_to_arduino(QString command)
{
    if(arduino->isWritable())
    {
        arduino->write(command.toStdString().c_str());
        return;
    }
    else{
        ui->usbport_lineEdit->clear();
        ui->usbport_lineEdit->insert("Couldn't write to Arduino");
    }
}

void robotic_mover::flitefile()
{
    QString prg = "padsp"; //padsp was added so oss systems will play flite 2.0
    QString prog = "flite";
    QString readfile ="-f";
    QString voicecall = "-voice";

    QString setstuff = "--seti";
    QStringList argu;


    argu<<prog<<voicecall<<voice<<setstuff<<readfile<<filepath;


    QProcess *flt1 = new QProcess(this);
        flt1->start(prg,argu);
        ttsline.clear();

    talker_flag=0;
    sleep(3);

}

void robotic_mover::mic_control()
{
    QString line = asrline;
    asrline.clear();

    if(line.contains("UP")==true)
    {
       ui->mic_on_checkBox->setChecked(true);
       ttsline="Robotic mover is awaiting your command";
       greet();
    }
    else if(line.contains("DOWN")==true)
    {
        write_to_arduino("S");
        ui->mic_on_checkBox->setChecked(false);
        ttsline="Robotic mover is standing down!";
        greet();
    }
}

void robotic_mover::do_turn()
{
    // code for Arduino turns will be here
    QString line = asrline;
    asrline.clear();
    if(forward_flag==true || ui->backup_turn_checkBox->isChecked()==true)
    {
        if(line.contains("RIGHT")==true)
        {
            write_to_arduino("I");
        }
        if(line.contains("LEFT")==true)
        {
            write_to_arduino("G");
        }
    }
}

void robotic_mover::hard_turn()
{
    // code for Arduino turns will be here
    QString line = asrline;
    asrline.clear();

    if(line.contains("RIGHT")==true)
    {
        write_to_arduino("R2");
    }
    if(line.contains("LEFT")==true)
    {
        write_to_arduino("L2");
    }

}

void robotic_mover::ease_turn()
{
    // code for Arduino turns will be here
    QString line = asrline;
    asrline.clear();

    if(line.contains("RIGHT")==true)
    {
        write_to_arduino("J");  // code on the arduino will contain a delay in J which
                                // will allow left wheel to travel more pushing vehicle
        write_to_arduino("F");  // to right after delay vehicle goes forward again
    }
    else if(line.contains("LEFT")==true)
    {
        write_to_arduino("H");  // code on the arduino will contain a delay in H which
                                // will allow left wheel to travel more pushing vehicle
        write_to_arduino("F");  // to right after delay vehicle goes forward again

    }

}


void robotic_mover::stop_progress()
{
    write_to_arduino("S");


}

void robotic_mover::parse_speaker()
{
    QString line = asrline;
    asrline.clear();

    if(line.contains("ON")==true)
    {
        ui->speaker_on_checkBox->setChecked(true);
        ttsline = "Speaker is On!";
        greet();

    }

    if(line.contains("OFF")==true)
        ui->speaker_on_checkBox->setChecked(false);
}

void robotic_mover::setup_boswasi()
{

        ttsline = "Stopping Robotic Mover to go to Boswasi! ";
        greet();



        QProcess *boswasi_call = new QProcess (this);
        QString prog = "boswasi";

        boswasi_call->startDetached(prog);
        sleep(3);
        QCoreApplication::exit();

}
